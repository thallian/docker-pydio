[Pydio](http://pydio.com/) server with ldap authentication

# Volumes
- `/var/lib/pydio/data`

# Environment Variables
## SSMTP_MAIL_RELAY
Hostname and port for the used smtp relay (for example `mail.example.com:587`).

## SSMTP_USER
User to authenticate agains the smtp relay.

## SSMTP_PASSWORD
Password to authenticate agains the smtp relay.

## SSMTP_AUTH_METHOD
- default: LOGIN

Which authentication mechanism to use for the smtp relay.

## SSMTP_USE_STARTTLS
- default: yes

Whether to use starttls for the smtp relay.

# Ports
- 80
