FROM registry.gitlab.com/thallian/docker-php7-fpm:master

ENV FPMUSER nginx
ENV FPMGROUP nginx
ENV LC_ALL en_us.UTF-8

ENV VERSION 8.0.2

RUN apk --no-cache add \
    libressl \
    nginx \
    ssmtp \
    mailx \
    php7 \
    php7-json \
    php7-dom \
    php7-session \
    php7-exif \
    php7-gd \
    php7-openssl \
    php7-mcrypt \
    php7-intl \
    php7-opcache \
    php7-iconv \
    php7-pdo_pgsql \
    php7-pgsql \
    php7-zlib \
    php7-ldap \
    php7-ctype \
    php7-imap

RUN mkdir /var/lib/pydio

RUN wget -qO- https://download.pydio.com/pub/core/archives/pydio-core-${VERSION}.tar.gz | tar -xz -C /var/lib/pydio --strip 1

RUN chown -R nginx:nginx /var/lib/pydio
RUN mkdir /run/nginx

ADD /rootfs /

VOLUME /var/lib/pydio/data
